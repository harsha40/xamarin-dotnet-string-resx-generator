const readline = require('readline');
const fs = require('fs');

// convert string to words
function toWords(input) {							
    var regex = /[A-Z\xC0-\xD6\xD8-\xDE]?[a-z\xDF-\xF6\xF8-\xFF]+|[A-Z\xC0-\xD6\xD8-\xDE]+(?![a-z\xDF-\xF6\xF8-\xFF])|\d+/g;
    return input.match(regex);
 }

 const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

function generateStringResFromViewFile(path){

    const fileReader = readline.createInterface({
        input: fs.createReadStream(path),
        output: false,
        console: false
    });

    //Find All Texts
    var regex = /(?!\n?Text=\"\{x)\n?Text=\"*/

    fileReader.on('line', function(line){
        var result = line.match(regex);

        if(result!== null){
            line = line.trim()
            console.log(value = line.substr(line.search("Text=")+5,line.length));

            const words=toWords(value);

            value = value.substr(1,value.length-2);

            let CapitalizedKey = "";
            words.forEach(word => {
                CapitalizedKey = CapitalizedKey + (capitalize(word));
            });

            console.log(CapitalizedKey, value);

            let entry = '<data name=\"'+CapitalizedKey+'\" xml:space=\"preserve\">\n\t<value>'+value+'</value>\n</data>\n';

            // write to a new file named 2pac.txt
            fs.appendFile('Strings.resx', entry, (err) => {
                // throws an error, you could also catch it here
                if (err) throw err;
                // success case, the file was saved
                console.log('Entry saved!');
            });
        }
    });
} 

function generateStringsResx() {
    fs.readdir("./Views", function (err, files) {
        //handling error
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        } 
        //listing all files using forEach
        files.forEach(function (file) {
            // Do whatever you want to do with the file
            console.log('./Views/'+file); 
            generateStringResFromViewFile('./Views/'+file);
        });
    });
}

generateStringsResx();